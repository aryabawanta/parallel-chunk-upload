1. Install dependencies: `npm i`
2. Copy `.env.local.example` into `.env.local` and change the values as you want
3. Run the app by running `npm run dev`