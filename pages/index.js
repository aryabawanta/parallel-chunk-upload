import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { useState, useEffect } from "react"
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { Dropzone } from "dropzone"

export default function Home() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [token, setToken] = useState();

  const handleLoginSubmit = async (e) => {
    e.preventDefault();
    const api_url = process.env.API_URL;
    const withCredentials = true; // set this to false if you don't want to send/save cookies
    const res = await axios.post(
      `${api_url}/v1/auth/jwt/token`,
      {
        username,
        password
      },
      {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials
      }
    );
    const { access_token } = res.data;
    setToken(access_token)
  };

  useEffect(() => {
    const element = document.getElementById("dropzone")
    if (element) {
      const dropzone = new Dropzone("#dropzone", {
        maxFilesize: 100, // default filesize in MB
        url: `${process.env.API_URL}/v1/files`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        withCredentials: true,
        acceptedFiles: ".step,.stp,.dxf",

        // for chunks
        paramName: "chunk",
        chunking: true,
        // Every 1 MB of file upload takes 10s to upload (5 MB takes 50s, 10 MB takes 100s, 30 MB takes 300s, etc.)
        // Meaning that the larger the chunk size, the longer the upload process
        chunkSize: 5_000_000, // 5 MB, change this to your liking
        retryChunks: true,
        retryChunksLimit: 3,
        parallelChunkUploads: true,
        forceChunking: true
      })
      dropzone.on("sending", (_, __, formData) => {
        const file_token = formData.get("dzuuid")
        const index = formData.get("dzchunkindex")
        formData.append("file_token", file_token || uuidv4())
        formData.append("index", index || "0")
        
        // remove unnecessary formData
        formData.delete("dzuuid")
        formData.delete("dzchunkindex")
        formData.delete("dztotalfilesize")
        formData.delete("dzchunksize")
        formData.delete("dztotalchunkcount")
        formData.delete("dzchunkbyteoffset")
      })

      dropzone.on("success", async (file, _) => {
        const { upload } = file
        const payload = {
          file_token: upload.uuid,
          total_chunks: upload.totalChunkCount,
          file_name: file.name,
        }
        await axios.post(
          `${process.env.API_URL}/v1/files/complete`,
          payload,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${token}`,
            },
            withCredentials: true
          }
        );
      })
    }
  })

  return (
    <div className={styles.container}>
      <Head>
        <title>Parallel Chunks</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div className={styles.grid}>
          {!token ?
            (<form method="post">
              <div className="container">
                <label htmlFor="uname" style={{ marginRight: "10px" }}><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required onChange={e => setUsername(e.target.value)} />
                <br />
                <br />
                <label htmlFor="psw" style={{ marginRight: "10px" }}><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" onChange={e => setPassword(e.target.value)} required />
                <br />
                <br />
                <button
                  type="submit"
                  onClick={handleLoginSubmit}
                >
                  Login
                </button>
              </div>
            </form>) : (
              <div>
                <h1>Logged in</h1>
                <div className="dropzone" id='dropzone' />
              </div>
            )
          }
        </div>
      </main>

      <style jsx>{`
        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        footer img {
          margin-left: 0.5rem;
        }
        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
          text-decoration: none;
          color: inherit;
        }
        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
        * {
          box-sizing: border-box;
        }
        .dropzone {
          border: 1px solid black;
        }
      `}</style>
    </div>
  )
}
